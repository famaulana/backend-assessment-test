<?php

namespace App\Services;

use App\Models\Loan;
use App\Models\ReceivedRepayment;
use App\Models\User;
use App\Models\ScheduledRepayment;
use App\Http\Resource\LoanResource;
use Carbon\Carbon;

class LoanService
{
    /**
     * Create a Loan
     *
     * @param  User  $user
     * @param  int  $amount
     * @param  string  $currencyCode
     * @param  int  $terms
     * @param  string  $processedAt
     *
     * @return Loan
     */
    public function createLoan(User $user, int $amount, string $currencyCode, int $terms, string $processedAt): Loan
    {
        
        //Check if user have any loans
        $statusLoan = Loan::where('user_id', $user->id)->where('status', 'due')->get();
        if(!$statusLoan->first()){
            $loanModels = new Loan();
            //Insert loans
            $loan = $loanModels->create([
                'user_id' => $user->id,
                'amount' => $amount,
                'terms' => $terms,
                'status' => 'due',
                'outstanding_amount' => 0,
                'currency_code' => $currencyCode,
                'processed_at' => $processedAt,
            ]);

            $scheduledRepayment = new ScheduledRepayment();
            //Insert Scheduled Payment
            $counter = 1;
            for($i = 0; $i < $terms; $i++){
                $date = new Carbon($loan->created_at);
                $scheduledRepayment->create([
                    'loan_id' => $loan->id,
                    'created_at' => $date->addMonth($counter)
                ]);
                $counter++;
            }

            return $loan;
        }
        $loan = $statusLoan->first();

        return $loan;
    }

    /**
     * Repay Scheduled Repayments for a Loan
     *
     * @param  Loan  $loan
     * @param  int  $amount
     * @param  string  $currencyCode
     * @param  string  $receivedAt
     *
     * @return ReceivedRepayment
     */
    public function repayLoan(Loan $loan, int $amount, string $currencyCode, string $receivedAt): Loan
    {
        //Check if amount enough for repayment loan
        if($amount < ($loan->amount/$loan->terms)){
            return [
                "code" => 3,
                "data" => $loan
            ];
        }else{
            $scheduledRepaymentData = ScheduledRepayment::where('loan_id', $loan->id)->where('deleted_at', null)->get();
    
            //Check if any repayment loans
            if(!$scheduledRepaymentData->first()){
                if($loan->amount < $loan->outstanding_amount){
                    $loan->update([
                        'status' => "repaid",
                        'deleted_at' => Carbon::now()->addYear(),
                    ]);
                }else{
                    $loan->update([
                        'status' => "",
                        'deleted_at' => Carbon::now()->addYear(),
                    ]);
                }
                return [
                    "code" => 3,
                    "data" => $loan
                ];
            }else{
                //Insert Received Repayment
                $receivedRepaiment = new ReceivedRepayment();
                $receivedRepaiment->create([
                    'loan_id' => $loan->id
                ]);

                //Updated loans detail
                if($loan->amount == $amount+$loan->outstanding_amount){
                    $loan->update([
                        'outstanding_amount' => $amount+$loan->outstanding_amount,
                        'status' => "",
                        'updated_at' => Carbon::now()->addYear(),
                        'deleted_at' => Carbon::now()->addYear(),
                    ]);
                }else if($amount+$loan->outstanding_amount > $loan->amount){
                    $loan->update([
                        'outstanding_amount' => $amount+$loan->outstanding_amount,
                        'status' => "repaid",
                        'updated_at' => Carbon::now()->addYear(),
                        'deleted_at' => Carbon::now()->addYear(),
                    ]);
                }else{
                    $loan->update([
                        'outstanding_amount' => $amount+$loan->outstanding_amount,
                        'updated_at' => Carbon::now()->addYear(),
                    ]);
                }
                
                //Delete scheduled after paid
                $termPaid = $loan->amount / $loan->terms;
                $paid = $loan->outstanding_amount;
                for($i = 0; $i < $loan->terms; $i++){
                    if($paid > $termPaid){
                        $scheduledRepayment = new ScheduledRepayment();
                        $return = $scheduledRepayment->where('id', $scheduledRepaymentData->first()->id)->update([
                            'deleted_at' => Carbon::now()->addYear()
                        ]);
                    }else{
                        break;;
                    }
                    $paid = $paid-$termPaid;
                }
        
                return [
                    "code" => 0,
                    "data" => $loan
                ];
            }
        }

    }
}
