<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Loan extends Model
{

    use HasFactory;
    
    public const STATUS_DUE = 'due';
    public const STATUS_REPAID = 'repaid';

    public const STATUS = [
        self::STATUS_DUE,
        self::STATUS_REPAID,
    ];
    
    public const TERMS_3 = '3';
    public const TERMS_6 = '6';

    public const TERMS = [
        self::TERMS_3,
        self::TERMS_6,
    ];

    public const CURRENCY_IDR = 'IDR';
    public const CURRENCY_SGD = 'SGD';
    public const CURRENCY_THB = 'THB';
    public const CURRENCY_VND = 'VND';

    public const CURRENCIES = [
        self::CURRENCY_IDR,
        self::CURRENCY_SGD,
        self::CURRENCY_THB,
        self::CURRENCY_VND,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'terms',
        'outstanding_amount',
        'currency_code',
        'processed_at',
        'status',
        'deleted_at',
    ];

    /**
     * A Loan belongs to a User
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A Loan has many Scheduled Repayments
     *
     * @return HasMany
     */
    public function scheduledRepayments()
    {
        return $this->hasMany(ScheduledRepayment::class, 'loan_id');
    }
}
