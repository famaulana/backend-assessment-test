<?php

namespace App\Providers;

use App\Models\DebitCard;
use App\Models\DebitCardTransaction;
use App\Models\Loan;
use App\Models\ReceivedRepayment;
use App\Policies\DebitCardPolicy;
use App\Policies\DebitCardTransactionPolicy;
use App\Policies\LoanPolicy;
use App\Policies\RecivedPaymentPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        DebitCard::class => DebitCardPolicy::class,
        DebitCardTransaction::class => DebitCardTransactionPolicy::class,
        Loan::class => LoanPolicy::class,
        ReceivedRepayment::class => RecivedPaymentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
