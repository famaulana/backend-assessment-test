<?php

namespace App\Http\Requests;

use App\Models\DebitCard;
use App\Models\DebitCardTransaction;
use Illuminate\Foundation\Http\FormRequest;

class DebitCardTransactionShowIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $debitCard = DebitCard::find($this->input('debit_card_id'));

        return $debitCard && $this->user()->can('view', $debitCard);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'debit_card_id' => 'required|integer|exists:debit_cards,id',
        ];
    }
    public function messages(){
        return [
            'debit_card_id.required' => "is_active is required",
            'debit_card_id.integer' => "is_active must integer",
            'debit_card_id.exists' => "is_active does'nt exist",
        ];
    }
}
