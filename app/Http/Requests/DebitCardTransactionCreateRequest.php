<?php

namespace App\Http\Requests;

use App\Models\DebitCard;
use App\Models\DebitCardTransaction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DebitCardTransactionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $debitCard = DebitCard::find($this->input('debit_card_id'));

        return $debitCard && $this->user()->can('create', [DebitCardTransaction::class, $debitCard]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'debit_card_id' => 'required|integer|exists:debit_cards,id',
            'amount' => 'required|integer',
            'currency_code' => [
                'required',
                Rule::in(DebitCardTransaction::CURRENCIES),
            ],
        ];
    }

    public function messages(){
        return [
            'debit_card_id.required' => "debit_card_id is required",
            'debit_card_id.integer' => "debit_card_id must integer",
            'debit_card_id.exists' => "debit_card_id not exist",
            'amount.required' => "amount is required",
            'amount.integer' => "amount must integer",
            'currency_code.required' => "currency_code is required",
        ];
    }
}
