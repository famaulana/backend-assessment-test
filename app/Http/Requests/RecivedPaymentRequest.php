<?php

namespace App\Http\Requests;

use App\Models\ReceivedRepayment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RecivedPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('create', ReceivedRepayment::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|integer',
            'currency_code' => [
                'required',
                Rule::in(ReceivedRepayment::CURRENCIES),
            ],
        ];
    }
}
