<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use App\Http\Requests\LoanRequest;
use App\Http\Requests\LoanShowRequest;
use App\Http\Requests\RecivedPaymentRequest;
use App\Http\Resources\LoanResource;
use App\Http\Resources\ReceivedRepaymentResource;
use App\Services\LoanService;
use App\Models\User;
use App\Models\Loan;
use App\Models\ScheduledRepayment;
use Carbon\Carbon;

class LoanController extends BaseController
{
    public function index(LoanShowRequest $request): JsonResponse
    {
        $loan = Loan::where('user_id', $request->user()->id)->get();
        
        $schedule = ScheduledRepayment::where('loan_id', $loan->first()->id)->get();

        return response()->json(["code" => "0", "info" => "", "data" => ["loan" => $loan, "scheduleRepayment" => LoanResource::collection($schedule)]], HttpResponse::HTTP_OK);
    }

    public function store(LoanRequest $request, LoanService $loanService)
    {
        $loan = $loanService->createLoan( $request->user(), $request->input('amount'), $request->input('currency_code'), $request->input('terms'), Carbon::now()->addYear());

        return response()->json(["code" => "0", "info" => "Your loan already created", "data" => $loan], HttpResponse::HTTP_CREATED);
    }

    public function recivedPayment(RecivedPaymentRequest $request, Loan $loan, LoanService $loanService)
    {
        $repayment = $loanService->repayLoan($loan, $request->input('amount'), $request->input('currency_code'), Carbon::now()->addYear());

        if($repayment->amount == $loan->amount){
            return response()->json(["code" => "3", "info" => "You must pay at least ".$loan->amount/$loan->terms], HttpResponse::HTTP_OK);
        }

        return response()->json(["code" => "0", "data" => $repayment], HttpResponse::HTTP_OK);
    }

}
;