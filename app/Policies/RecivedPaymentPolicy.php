<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ReceivedRepayment;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecivedPaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function create(User $user): bool
    {
        return true;
    }
}
