<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Loan;
use App\Models\scheduledRepayment;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    public function view(User $user, ?ScheduledRepayment $scheduledRepayment = null): bool
    {
        if (!$scheduledRepayment) {
            return true;
        }

        return $user->is($debitCard->user);
    }

    public function create(User $user): bool
    {
        return true;
    }
}
