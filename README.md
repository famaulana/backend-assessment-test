# Backend Assessment Test

# How to clone Project
1. Clone repository
2. Make .env file
3. Run Composer Install
4. Run php artisan key:generate
5. Run php artisan migrate
6. Run php artisan serve.

Note : If there any error check if passport package already installed and get token.

#List API's : 
There is file collection from postman named "Api-list.postman_collection.json" in this repo.

Note : Don't forget to insert user account to database on table "user" for access the api's
